from pygame import *  # All graphics programs will need this
from tkinter import *  # Imports the tinker library to create the open and save dialogue
from tkinter import filedialog  # Imports the actual dialogue
from math import *  # Used for easier calculations
from random import *  # Used for generating randomness
from threading import *  # Used for creating a real loading screen
import os  # Setting the window's initial position
import pygame  # Used to prevent problems with library name conflicts
from collections import *
import time as ty

os.environ['SDL_VIDEO_WINDOW_POS'] = '5,30'  # Sets the position of the window to (5, 30) so the user wont need to move the window every time

init()  # Init the Pygame module
# r   g b
RED = Color(255, 0, 0)  # Some preset colors for generate the base of the program
GREEN = Color(0, 255, 0)
BLUE = Color(0, 0, 255)
BLACK = Color(0, 0, 0)
WHITE = Color(255, 255, 255)

foregroundRect = Rect(20, 645, 40, 40)  # Sets the foreground and background Rects
backgroundRect = Rect(70, 645, 40, 40)
foreground = BLACK  # Sets the default foreground and background Colors
background = WHITE
activefb = "foreground"  # Sets the current active color to foreground. The user can change between background and foregrounds colors
currentColor = foreground  # Sets the current color to the active color selected

pencilRect = Rect(20, 80, 40, 40)  # Creates the Rectangles for the tools (needed for collision)
brushRect = Rect(70, 80, 40, 40)
eraserRect = Rect(20, 140, 40, 40)
fillRect = Rect(70, 140, 40, 40)
dropperRect = Rect(20, 200, 40, 40)
markerRect = Rect(70, 200, 40, 40)
textRect = Rect(20, 260, 40, 40)
sprayRect = Rect(70, 260, 40, 40)
circleRect = Rect(20, 320, 40, 40)
rectRect = Rect(70, 320, 40, 40)
polygonRect = Rect(20, 380, 40, 40)
lineRect = Rect(70, 380, 40, 40)
sepiaRect = Rect(20, 440, 40, 40)
grayRect = Rect(70, 440, 40, 40)

spriteleftRect = Rect(560, 810, 20, 20)  # Creates the Rectangles for the stamp library and the page change arrows
spriterightRect = Rect(1230, 810, 20, 20)
spriteBarRect = Rect(600, 785, 610, 70)

bkleftRect = Rect(560, 720, 20, 20)  # Creates the Rectangles for the background library and the page change arrows
bkrightRect = Rect(1230, 720, 20, 20)
bkBarRect = Rect(600, 695, 610, 70)

menu = True  # A boolean variable that states which mode the program is in (menu/paint)
size = (1280, 960)  # The size of the window
canvasRect = Rect(300, 20, 960, 650)  # The size of the canvas

screen = display.set_mode(size)  # Creating the display with the dimensions defined by side

display.set_caption("Basic Paint")  # Sets the caption of the window

root = Tk()  # Initializing the tinker module
root.withdraw()  # Withdraws the main menu because we only need the open/save dialogue

Drawing = False  # A boolean used to determine when to copy the screen for the undo operation
tool = "no tool"  # The tool current active (Default: "no tool")

thickness = othickness = 10  # Sets the default thickness for the program
andyfont = pygame.font.Font("images/font/Andy.ttf", thickness)  # Loading the font

brushHeadTemplate = Surface((201, 201), SRCALPHA)  # Creating a alpha surface for the marker tool
toolRequire = False  # A boolean used to determine when to copy the screen

# undo redo
undoRect = Rect(20, 20, 40, 40)  # Creating the Rect
undoico = transform.smoothscale(image.load("images/icon/undo.png"), (30, 30)).convert_alpha()  # Loading the image and transforming it to 30, 30 and the image is converted for faster bliting
redoRect = Rect(70, 20, 40, 40)
redoico = transform.smoothscale(image.load("images/icon/redo.png"), (30, 30)).convert_alpha()

# Save Load
saveRect = Rect(120, 20, 40, 40)
saveico = transform.smoothscale(image.load("images/icon/save.png"), (30, 30)).convert_alpha()
openRect = Rect(170, 20, 40, 40)
openico = transform.smoothscale(image.load("images/icon/open.png"), (30, 30)).convert_alpha()

#################################################################
# Loading sound files and icon for music

musicRect = Rect(1230, 910, 50, 50)  # Creating a rect for the icon
musicIcon = transform.smoothscale(image.load("images/music.png"), (50, 50)).convert_alpha()  # Loading the icon and transforming it to 50, 50
musicOffIcon = transform.smoothscale(image.load("images/musicOff.png"), (50, 50)).convert_alpha()

SONG_END = USEREVENT + 1  # Creating an event number to be set when the music is over

mixer.music.set_endevent(SONG_END)  # Setting the event number when the music is finished to SONG_END

soundLib = ["(" + str(i) + ")" for i in range(1, 41)]  # Using a list to store all of the file names


shuffle(soundLib)  # Randomizing the list so that the order of the music will be random each time

#################################################################
# loading screen
screen.fill(WHITE)  # Filling the screen with the color white
loadingCircle = []  # A list where the animations of the loading circle will be saved

for i in range(1, 15):  # A loop to load all of the files
    temp = image.load("images/loading/frame (" + str(i) + ").png").convert_alpha()  # Loading the file
    loadingCircle.append(temp)  # Adding the file to the end of the list


def loading_screen(screen):  # The loading screen function (used by another thread)
    t = current_thread()  # Setting t as the current thread
    loadab = loadingCircle[0].get_rect()  # Get a rect with the same height and width of the frame
    posx = size[0]//2 - loadab.w//2  # Finding the X position to place the frames
    posy = size[1]//2 - loadab.h//2  # Finding the Y position to place the frames

    while getattr(t, "do_run", True):  # Checking if the attribute "do_run" of thread t is true or not
        time.Clock().tick(60)  # Setting the tick so that the circle will spin around slowly
        screen.blit(loadingCircle[0], (posx, posy))  # Bliting the frame on to the screen
        loadingCircle.append(loadingCircle.pop(0))  # Adding the current frame to the end of the queue to run the next frame
        display.flip()  # update the screen

p = Thread(target=loading_screen, args=(screen,))  # Setting p to a new thread
p.start()  # Starting the thread

#################################################################
# Tool menu

ToolRect = Rect(10, 70, 120, 500)  # Tool rectangle
ToolSurface = transform.scale(image.load("images/toolbox.png"), (120, 440)).convert_alpha()  # The background of the tool box

toolIconList = ["pencil", "brush", "eraser", "painter", "dropper", "marker", "TextTool", "spraycan", "circle", "rect", "polygon", "line", "sepia", "grayscale"]  # A list of all the icon names for batch loading
toolSurfaceLib = {}  # A dictionary used as a library the surfaces

for i in range(0, len(toolIconList), 2):  # A for loop for mass loading
    try:  # Try except statement to ignore errors
        temp = image.load("images/icon/"+toolIconList[i]+".png")  # Loading the icon
        temp = transform.smoothscale(temp, (30, 30))  # Transforming the icon to 30, 30
        ToolSurface.blit(temp, (17, 15+i//2*60))  # Bliting the icon on to the tool surface to reduce the number of pixels to blit each time the program runs

        toolSurfaceLib[toolIconList[i]] = temp  # Adding the icon to the Library

        temp = image.load("images/icon/"+toolIconList[i+1]+".png")  # Same as above, but at an increased x coordinate
        temp = transform.smoothscale(temp, (30, 30))
        ToolSurface.blit(temp, (67, 15+i//2*60))

        toolSurfaceLib[toolIconList[i+1]] = temp
    except:  # Error
        pass  # Continue the loop

toolBorder = image.load("images/toolBorder.png").convert_alpha()  # Loading the tool border image
toolBorder = transform.scale(toolBorder, (41, 41))  # Transforming the image to the correct size

#################################################################
# Sliders menu/functions/color picker

Utility_Rect = Rect(10, 600, 260, 365)  # Creating the utility rectangle
Utility_Surf = transform.scale(image.load("images/toolBox.png"), (260, 365)).convert_alpha()  # Loading and transforming the background image

colorpicker = image.load("images\color-picker.png")  # Loading the colour picker image
colorpickerRect = colorpicker.get_rect()  # Getting a rect of the image
colorpickerRect.x, colorpickerRect.y = Utility_Rect.w - colorpickerRect.w, Utility_Rect.h - colorpickerRect.h  # Calculating the x and y position of the colour picker on the utility rectangle
thicknessLineX = (38, 238)  # Thickness slider for the tools (x points only)

draw.line(Utility_Surf, GREEN, (thicknessLineX[0]-10, 20), (thicknessLineX[1]-10, 20), 3)  # Drawing the line portion of the slider
Utility_Surf.blit(colorpicker, (colorpickerRect.x, colorpickerRect.y))  # Bliting the color picker with pre-calculated x and y

colorpickerRect.x, colorpickerRect.y = 10, 695  # Setting the x and y of the rect in relation to the actual screen

#################################################################
# Loading stamps

spriteLoadingList = ["spritesheet", "character"]  # File names for cutting. A sprite sheet is loaded to increase load speed
backgroundLoadingList = ["background"]  # Same as above but for background

spriteLibrary = {}  # Library for the cut stamps
backLibrary = {}

currentx = 10  # Sets a starting index for the stamps
for i in spriteLoadingList:  # Loading each sprite sheet
    spritesheet = image.load("images/stamps/"+i+".png").convert_alpha()  # Loading the sprite sheet and converting it to make bliting faster
    positions = open("images/stamps/"+i+".txt", "r")  # Opening a file with all of the cords

    for line in positions.readlines():  # Cutting each stamp
        details = line.strip("\n").strip("[").strip("]").split(",")  # Deleting some characters to help python read the file
        if len(details) > 4:  # Dealing with more than 4 things provided per line
            x, y, dx, dy = details[1:]  # assigning x, y, dx, dy with the information
        else:
            x, y, dx, dy = details  # Same as above
        spriteLibrary[currentx] = spritesheet.subsurface((int(x), int(y), int(dx), int(dy))).copy()  # Adding sprite to the library
        currentx += 60

    positions.close()

currentx = 10  # Reset for Background
for i in backgroundLoadingList:  # Same as above but for background
    spritesheet = image.load("images/stamps/"+i+".png").convert_alpha()
    positions = open("images/stamps/"+i+".txt", "r")

    for line in positions.readlines():
        name, x, y, dx, dy = line.strip("\n").split(",")
        backLibrary[currentx] = transform.scale(spritesheet.subsurface((int(x), int(y), int(dx), int(dy))).copy(), (960, 650))
        currentx += 60

    positions.close()


#################################################################
# Creating Sprite Selection Surfaces

SpriteSurfaces = {}  # A library of surfaces with the sprites on them to increase blitting speed(2 image per frame vs 11 images per frame)
spriteSurfaceNumber = []  # A list of all the sprite surface numbers
template = Surface((610, 70), SRCALPHA)  # The background surface
template.fill((0, 0, 0, 0))  # Filling it with blank
counter = 0
slideNumber = 0
SpriteXCord = list(spriteLibrary.keys())  # Getting the name of all the sprites
SpriteXCord.sort()

'''
This part of the script transforms and centers the stamps in a 50 by 50 area. And only blits 10 stamps on each surface.
The x coordinate increases each time to blit the images on different sections

The second loop does the exact same thing except its doing the backgrounds
'''

for k in SpriteXCord:
    spritesurf = spriteLibrary[k]
    spriteWidth = spritesurf.get_width()
    spriteHeight = spritesurf.get_height()
    scale = 50/max(spriteWidth, spriteHeight)  # Scale to transform by

    spritesurf = transform.scale(spritesurf, (int(spriteWidth*scale), int(spriteHeight*scale)))  # Transforming the image
    template.blit(spritesurf, (k+25-(spriteWidth//2)*scale-(slideNumber*600), 10+25-(spriteHeight//2)*scale))  # Bliting and centering it
    counter += 1

    if counter == 10 or k == SpriteXCord[-1]:
        SpriteSurfaces[slideNumber] = template.copy()
        spriteSurfaceNumber.append(slideNumber)
        template.fill((0, 0, 0, 0))
        counter = 0
        slideNumber += 1

bkSurfaces = {}
bkSurfaceNumber = []
counter = 0
slideNumber = 0
bkXCord = list(backLibrary.keys())
bkXCord.sort()

for k in bkXCord:
    bksurf = backLibrary[k]
    bkWidth = bksurf.get_width()
    bkHeight = bksurf.get_height()
    scale = 50 / max(bkWidth, bkHeight)

    bksurf = transform.scale(bksurf, (int(bkWidth * scale), int(bkHeight * scale)))
    template.blit(bksurf, (k - (slideNumber * 600), 60-bkHeight*scale))
    counter += 1

    if counter == 10 or k == SpriteXCord[-1]:
        bkSurfaces[slideNumber] = template.copy()
        bkSurfaceNumber.append(slideNumber)
        template.fill((0, 0, 0, 0))
        counter = 0
        slideNumber += 1

#################################################################
# Tool functions


'''
This function takes the distance between dx and dy(minimum distance is one). After it loops through the distance and finds the x and y in proportion to the starting point
+ the ratio * dx/dy. This is basically dividing the line in to smaller bits. A circle is drawn for every pixel of x and y.


The sprymode option actives the spray part. Instead of drawing a circle, it get 2 random points and test for whether those points are less than or equal to the radius.
(Distance between the point and center of the circle). Then it sets the pixel at that position to the color
'''


def continuous_line(srf, color, start, end, radius=1, sprymode=False):
    dx = end[0]-start[0]
    dy = end[1]-start[1]
    distance = max(1, hypot(dx, dy))
    for i in range(int(distance)):
        x = int(start[0]+i/distance*dx)
        y = int(start[1]+i/distance*dy)
        if not sprymode:
            draw.circle(srf, color, (x, y), radius)
        else:
            cirx, ciry = randint(-1 * radius, radius), randint(-1 * radius, radius)

            if hypot(cirx, ciry) <= radius:
                srf.set_at((cirx+x, ciry+y), color)

'''
Fill bucket

This function uses queue Flood fill method to fill a space. It takes a point (x, y), and remove that from the queue and set the color of that pixel to the color selected.
After it adds 4 more points into the list(the point above, below, to the left, and to the right) This process is done on those points and repeats until all
pixels in that area are changed.

Pixelarray is used to speed up the process. Instead of modifying a surface(slow) it now just change a couple of numbers in a large array(fast).
'''


def fill_bucket(x, y, newClr, oldClr, canvas):
    start = ty.time()
    visit_queue = deque([[x, y]])  # The queue
    pixels = PixelArray(screen)

    oldClr = (255 - oldClr.a) * 256 * 256 * 256 + oldClr.r * 256 * 256 + oldClr.g * 256 + oldClr.b
    newClr = (255 - newClr.a) * 256 * 256 * 256 + newClr.r * 256 * 256 + newClr.g * 256 + newClr.b

    if pixels[x, y] != newClr:

        while visit_queue:
            e = visit_queue.pop()
            w = e[:]

            if pixels[e[0], e[1]] == oldClr:
                while True:
                    w[0] -= 1
                    if pixels[w[0], w[1]] != oldClr:
                        w[0] += 1
                        break

                while True:
                    e[0] += 1
                    if pixels[e[0], e[1]] != oldClr:
                        e[0] -= 1
                        break

                for i in range(w[0], e[0]+1):
                    pixels[i, e[1]] = newClr
                    if pixels[i-1, e[1]+1] == oldClr:
                        visit_queue.append([i, w[1]+1])
                    if pixels[i-1, e[1]-1] == oldClr:
                        visit_queue.append([i, w[1]-1])

    print(ty.time()-start)

'''
Marker

Same as continuous line but with blitting instead of draw circle
'''


def marker(mx, my, omx, omy, brushhead, surface):
    dx = mx-omx
    dy = my-omy
    distance = max(1, hypot(dx, dy))
    for i in range(int(distance)):
        x = int(omx+i/distance*dx)
        y = int(omy+i/distance*dy)

        surface.blit(brushHead, (x, y))

'''
Text tool

Very simple function. Returns with a rendered text surface. Function is made to shorten the number of characters needed to be typed
'''


def text_tool(string, color):
    return andyfont.render(string, True, color)


polypoints = []  # Includes all the points to draw the polygon
polymode = False  # Is the tool active?


def draw_polygon(screen, mx, my, color, thickness):
    global polypoints  # Global is needed because the function needs to change something outside of the function
    polypoints.append([mx, my])

    for i in range(1, len(polypoints)):  # Draws the lines between the vertices
        continuous_line(screen, color, polypoints[i-1], polypoints[i], thickness)

    if len(polypoints) > 2:  # Closes the shape if it has more than 2 vertices
        continuous_line(screen, color, polypoints[0], polypoints[-1], thickness)
        draw.polygon(screen, color, polypoints, thickness)  # Creating a filled polygon if needed

'''
Regular

finds the max of dx and dy to create regular shapes
'''


def regular(dx, dy):
    if dx < 0 or dy < 0:  # Starts to test if one of the change is negative
        if dx < 0 and dy < 0:  # Both negative
            return [max(abs(dx), abs(dy))*-1, max(abs(dx), abs(dy))*-1]
        else:
            side = max(abs(dx), abs(dy))

            if side != abs(dx):  # dy is the max value and changed to neither positive or negative
                return [dy*-1, dy]
            else:
                return [dx, dx*-1]  # dx is negative

    return [max(dx, dy), max(dx, dy)]  # dx and dy are both positive


start_rect = []  # beginning point for the rectangle
rect_mode = False  # Is the tool active


def draw_rect(screen, mx, my, color, thickness, reg = False):
    global start_rect  # Global is needed because the function needs to change something outside of the function

    if len(start_rect) == 0:  # Setting the starting point of the rectangle
        start_rect = [mx, my]
    else:
        dx = mx - start_rect[0]
        dy = my - start_rect[1]

        if reg:
            dx, dy = regular(dx, dy)  # Getting the max of dx and dy

        draw.line(screen, color, [start_rect[0] - thickness // 2+1, start_rect[1]],  # Fix corner problems when the thickness is high
                  [start_rect[0] + dx + thickness // 2-1, start_rect[1]], thickness)

        draw.line(screen, color, [start_rect[0] - thickness // 2+1, my],  # Same as above
                  [start_rect[0] + dx + thickness // 2-1, my], thickness)

        draw.rect(screen, color, [start_rect[0], start_rect[1], dx, dy], thickness)

start_cir = []  # beginning point for the rectangle
cir_mode = False  # Is the tool active


def draw_cir(screen, mx, my, color, thickness, reg = False):
    global start_cir  # Global is needed because the function needs to change something outside of the function

    if len(start_cir) == 0:
        start_cir = [mx, my]
    else:
        dx = mx - start_cir[0]
        dy = my - start_cir[1]

        if reg:
            dx, dy = regular(dx, dy)

        try:  # Needed due to error caused by the delta of one of the cords not larger than the thickness
            if dx < 0 or dy < 0:
                if dx < 0 > dy:
                    dx *= -1
                    dy *= -1
                    draw.ellipse(screen, color, [start_cir[0] - dx, start_cir[1] - dy, dx, dy], thickness)

                elif dx < 0 < dy:
                    dx *= -1
                    draw.ellipse(screen, color, [start_cir[0] - dx, start_cir[1], dx, dy], thickness)

                elif dx > 0 > dy:
                    dy *= -1
                    draw.ellipse(screen, color, [start_cir[0], start_cir[1] - dy, dx, dy], thickness)

                else:
                    dy *= -1
                    draw.ellipse(screen, color, [start_cir[0] - dx, start_cir[1] - dy, dx, dy], thickness)

            else:
                draw.ellipse(screen, color, [start_cir[0], start_cir[1], dx, dy], thickness)
        except:
            pass

'''
Line function uses the same logic as the rect function
'''

start_line = []
line_mode = False


def line(screen, mx, my, color, thickness):
    global start_line
    if len(start_line) == 0:
        start_line = [mx, my]

    else:
        draw.line(screen, color, [start_line[0], start_line[1]], [mx, my], thickness)

'''
Run through every coordinate of the canvas, getting the color, and performing a algorithm to the colors before setting the colors back
'''


def sepia(surf, dim):
    for x in range(dim.w):
        for y in range(dim.h):
            r, g, b, a = screen.get_at((dim.x+x, dim.y+y))

            r2 = min(255, int(r * 0.393 + g * 0.769 + b * 0.189))
            g2 = min(255, int(r * 0.349 + g * 0.686 + b * 0.168))
            b2 = min(255, int(r * 0.272 + g * 0.534 + b * 0.131))

            screen.set_at((dim.x+x, dim.y+y), (r2, g2, b2, a))


def grayscale(surf, dim):  # Same as above but uses the grayscale algorithm instead
    for x in range(dim.w):
        for y in range(dim.h):
            r, g, b, a = screen.get_at((dim.x+x, dim.y+y))

            c1 = (r+g+b)//3

            screen.set_at((dim.x+x, dim.y+y), (c1, c1, c1, a))

#################################################################
# Other functions

'''
Load is set as a function to prevent errors if the user clicks cancel on the open file screen
'''


def load_image(path=""):
    readable = [("Readable file type", "*.bmp *.png *.jpeg *.jpg")]  # Accepted file types
    if path:
        file_path = path
    else:
        file_path = filedialog.askopenfilename(filetypes=readable)

    if file_path:  # If the user selected a file
        return image.load(file_path)
    else:
        return False  # If the user clicked cancel

'''
The save function has the same idea as the function above
'''


def save_image():

    subscreen = screen.subsurface(canvasRect)
    file_path = filedialog.asksaveasfilename(defaultextension=".png")

    if file_path:
        image.quit(subscreen, file_path)


def inidraw():  # Prepares the screen after the actual paint part of the program starts
    screen.blit(bkrd, (0, 0))  # draws background
    draw.rect(screen, WHITE, canvasRect)  # draws the canvas

################################################################
# Creating menu screen loading everything and transforming it to the correct size

menuPic = transform.scale(image.load("images/menu/menuBackround.png"), (1280, 960)).convert()
button_load = image.load("images/menu/load.png").convert_alpha()
button_load = transform.smoothscale(button_load, (button_load.get_width()//3, button_load.get_height()//3))

loadRect = button_load.get_rect()

button_loadHover = image.load("images/menu/loadSelected.png").convert_alpha()
button_loadHover = transform.smoothscale(button_loadHover, (button_loadHover.get_width()//3, button_loadHover.get_height()//3))

button_quit = image.load("images/menu/quit.png").convert_alpha()
button_quit = transform.smoothscale(button_quit, (button_quit.get_width()//3, button_quit.get_height()//3))

quitRect = button_quit.get_rect()

button_quitHover = image.load("images/menu/quitSelected.png").convert_alpha()
button_quitHover = transform.smoothscale(button_quitHover, (button_quitHover.get_width()//3, button_quitHover.get_height()//3))

button_start = image.load("images/menu/startPaint.png").convert_alpha()
button_start = transform.smoothscale(button_start, (button_start.get_width()//3, button_start.get_height()//3))

startRect = button_start.get_rect()

button_startHover = image.load("images/menu/startPaintSelected.png").convert_alpha()
button_startHover = transform.smoothscale(button_startHover, (button_startHover.get_width()//3, button_startHover.get_height()//3))

loadRect.x, loadRect.y = (size[0] // 2) - (button_load.get_width() // 2), size[1] // 2 + 200  # Setting x and y s for collision later
quitRect.x, quitRect.y = (size[0] // 2) - (button_quit.get_width() // 2), size[1] // 2 + 300
startRect.x, startRect.y = (size[0] // 2) - (button_start.get_width() // 2), size[1] // 2 + 100

cursor = transform.scale(image.load("images/Cursor_0.png"), (20, 20)).convert_alpha()  # cursor
cursorText = transform.scale(image.load("images/cursorText.png"), (20, 20)).convert_alpha()  # Cursor for text mode

menu = Surface((size[0], size[1]))

menu.blit(menuPic, (0, 0))
menu.blit(button_start, (startRect.x, startRect.y))  # bliting everything onto the menu beforehand for faster program speed
menu.blit(button_load, (loadRect.x, loadRect.y))
menu.blit(button_quit, (quitRect.x, quitRect.y))

################################################################
# Loading Main Paint

bkrd = transform.smoothscale(image.load("images/bkrd.png"), (1280, 960)).convert()

arrow = transform.scale(image.load("images/arrow.png"), (20, 20)).convert_alpha()
arrowSelected = transform.scale(image.load("images/selected arrow.png"), (20, 20)).convert_alpha()

Rarrow = transform.flip(arrow, True, False)  # fliping the arrows so that it faces the opposite direction
RarrowSelected = transform.flip(arrowSelected, True, False)

spriteBorder = transform.scale(toolBorder, (60, 60)).convert_alpha()
spriteBkrd = image.load("images/spriteLibb.png").convert_alpha()

################################################################

tool = "no tool"  # Tool active
omx, omy = 300, 300  # will be used for the pencil tool
text = ""  # Text written

adder = thickness*2  # Finding the X cord of the slider
thicknessSliderBox = Rect(thicknessLineX[0], 610, 10, 20)  # Setting the collision for the slider
thicknessLineRect = Rect(thicknessLineX[0], 610, 201, 20)

textx, texty = 0, 0  # Saving the coordinates that the text for the text tool will be at
textMode = False  # In tool active
running = True  # boolean variable
UsingTool = False  # Is the user using the tool

allowed = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '!', '"', '#', '$', '%', '&', "\\", "'", '(', ')', '*', '+', ',', '-', '.', '/', ':', ';', '<', '=', '>', '?', '@', '[', ']', '^', '_', '`', '{', '|', '}', '~', "'", "'"]
# A list of characters that is allowed to be typed into the program

p.do_run = False  # stopping the loading screen

spSelectedX = "none"  # The sprite selected
spSelectedPage = 0  # The page that the selected sprite is on
screen.fill(WHITE)  # Filling the screen white to create the first undo
undoList = [screen.subsurface(canvasRect).copy()]  # Getting the first undo screen(Blank)
redoList = []  # Redo list
drawn = False  # If the user actually drew something

mixer.music.load('music/ '+soundLib[-1]+'.ogg')  # Loading music
mixer.music.play()  # Playing music
volume = 1.0  # Defualt volume

mouse.set_visible(False)  # We don't want the mouse to show

while running:  # Main loop for the program
    shift_pressed = False  # If the user presses shift or not
    enter = False  # If enter is pressed
    click = False  # Get a single click from the user

    for evt in event.get():  # Event loop
        if evt.type == QUIT:  # Quiting the program
            running = False
        elif evt.type == MOUSEBUTTONDOWN:
            if evt.button == 1:  # Left click (Only once)
                click = True  # One click
                if not toolRequire:  # Some tools require canvas to not be copied every mouse down
                    canvasBuff = screen.subsurface(canvasRect).copy()  # Screen copy

        elif evt.type == MOUSEBUTTONUP:  # Releases mouse button
            ucopy = screen.subsurface(canvasRect).copy()  # Copy screen for undo
            if evt.button == 1:  # Left button released
                if drawn:  # Test if the user actually drew something.
                    drawn = False  # Set drawn to false (Prepares for the next draw)
                    redoList = []  # Clears the redo list
                    undoList.append(ucopy)  # Add canvas to the undo list

        elif evt.type == KEYDOWN:  # Keyboard input
            if evt.unicode in allowed:  # Check to make sure that the character is actually displayable (Example: the shift key is not able to be displayed)
                text += evt.unicode  # Gets the actual letter
            elif evt.key == K_RETURN:  # Enter key
                enter = True
            elif evt.key == K_BACKSPACE:  # Delete key
                try:  # Detecting if a error occurs
                    text = text[:len(text)-1]  # Remove the last letter of the word
                except:  # Error occurs
                    pass  # Continue on with the program

        elif evt.type == SONG_END:  # Start the second song if the first one ends
            mixer.music.load('music/ ' + soundLib[0] + '.ogg')  # Loads the music that's next in line
            mixer.music.play()  # Play the music
            mixer.music.set_volume(volume)  # Changing mute and unmute
            soundLib.append(soundLib.pop(0))  # Remove the song and place it back at the end

    mx, my = mouse.get_pos()  # Getting the mouse position on the screen

    mb = mouse.get_pressed()  # getting which button is pressed

    if menu:  # Displays the menu screen
        screen.blit(menu, (0, 0))  # blits the menu

        if volume == 1:  # blits the correct volume icon (On/Off)
            screen.blit(bkrd.subsurface(musicRect), musicRect)
            screen.blit(musicIcon, musicRect)  # blits music On icon
        elif volume == 0:
            screen.blit(bkrd.subsurface(musicRect), musicRect)
            screen.blit(musicOffIcon, musicRect)  # blits music Off icon

        if musicRect.collidepoint(mx, my):  # If the user hovers over the music rect
            if click and volume == 1:  # Change volume to off if on
                volume = 0
                mixer.music.set_volume(volume)  # Set the music's new volume

            elif click and volume == 0:  # Change volume to on if off
                volume = 1
                mixer.music.set_volume(volume)  # Set the music's new volume
        ##################################################################
        # Menu Buttons

        if startRect.collidepoint(mx, my):  # Hover over the button
            screen.blit(button_startHover, (startRect.x, startRect.y))  # Give some user feedback
            if click:  # Clicked on the button
                menu = False  # Menu off
                inidraw()  # Starts the paint part of the program

        if loadRect.collidepoint(mx, my):
            screen.blit(button_loadHover, (loadRect.x, loadRect.y))
            if click:
                filepath = load_image()  # Gets the image being loaded
                if filepath:  # Gets if the user actually opened a file
                    menu = False
                    inidraw()
                    drawn = True  # Screen has been updated, get a copy for undo
                    screen.set_clip(canvasRect)  # setting a clip so that the image being loaded wont cover the entire screen
                    screen.blit(filepath, canvasRect)  # Bliting the actual image

        if quitRect.collidepoint(mx, my):
            screen.blit(button_quitHover, (quitRect.x, quitRect.y))
            if click:
                menu = False
                break  # Breaks the program's main loop

    #########################################################################
    else:  # Paint program starts (after menu)
        screen.blit(bkrd.subsurface((undoRect.x, undoRect.y, 200, 60)), undoRect)  # Refreshes part of the background

        screen.blit(ToolSurface, (ToolRect.x, ToolRect.y))  # Blits the toolbox
        screen.blit(Utility_Surf, (Utility_Rect.x, Utility_Rect.y))  # Blits the Utility box

        #####################################################################
        # Loading sprite/background box

        screen.blit(spriteBkrd, (spriteleftRect.x, 775))
        screen.blit(SpriteSurfaces[spriteSurfaceNumber[0]], (600, 785))
        screen.blit(arrow, spriteleftRect)  # Left arrow
        screen.blit(Rarrow, spriterightRect)  # Right arrow

        screen.blit(spriteBkrd, (bkleftRect.x, 685))  # Background selector
        screen.blit(bkSurfaces[bkSurfaceNumber[0]], bkBarRect)
        screen.blit(arrow, bkleftRect)
        screen.blit(Rarrow, bkrightRect)

        #####################################################################
        # undo/redo/save/open buttons
        screen.blit(undoico, (undoRect.x + 7, undoRect.y + 7))  # Bliting the buttons at a more centered location
        screen.blit(redoico, (redoRect.x + 7, redoRect.y + 7))
        screen.blit(saveico, (saveRect.x + 7, saveRect.y + 7))
        screen.blit(openico, (openRect.x + 7, openRect.y + 7))
        #####################################################################
        if volume == 1:  # Updates the volume icon
            screen.blit(bkrd.subsurface(musicRect), musicRect)
            screen.blit(musicIcon, musicRect)
        elif volume == 0:
            screen.blit(bkrd.subsurface(musicRect), musicRect)
            screen.blit(musicOffIcon, musicRect)

        if undoRect.collidepoint(mx, my):  # Undo is pressed
            if click and len(undoList) > 1:# Check if the user clicked and if the undoList has anything left to prevent problem
                if textMode:
                    textMode = False
                    text = ""
                cop = undoList.pop()  # Gets and remove the last element
                screen.blit(undoList[-1], canvasRect)  # Gets the second element amd blit that
                redoList.append(cop)  # add to the redo list

            screen.blit(toolBorder, undoRect)  # User feedback

        elif redoRect.collidepoint(mx, my):  # Save method as above but for redo
            if click and len(redoList) > 0:
                if textMode:
                    textMode = False
                    text = ""
                cop = redoList.pop()
                screen.blit(cop, canvasRect)
                undoList.append(cop)

            screen.blit(toolBorder, redoRect)

        #####################################################################
        if key.get_mods() & KMOD_SHIFT:  # Check if shift has been pressed. A bit wise operator is used because 1 is True. Using this saves time.
            shift_pressed = True

        draw.rect(screen, (100, 255, 100), (thicknessSliderBox.x + adder, thicknessSliderBox.y, thicknessSliderBox.w, thicknessSliderBox.h))  # draws the actual slider

        if mb[0] == 1 and canvasRect.collidepoint(mx, my) and tool != "no tool":  # Testing if the user is using the tool
            UsingTool = True
        elif mb[0] == 0:  # only when the user releases the mouse, UsingTool is false
            UsingTool = False
        #####################################################################
        # Utility menu changes
        draw.rect(screen, foreground, foregroundRect)  # blits the foreground and background color
        draw.rect(screen, background, backgroundRect)

        if activefb == "foreground":  # Sets the active color
            screen.blit(toolBorder, foregroundRect)  # Get feedback on which color is selected
            draw.rect(screen, BLACK, backgroundRect, 2)  # Draw a black rect around the other one to avoid confusion
            currentColor = foreground  # Sets the current color
        else:
            screen.blit(toolBorder, backgroundRect)  # Same as above but for when the user selects the background color
            draw.rect(screen, BLACK, foregroundRect, 2)
            currentColor = background
        #####################################################################
        # Tool Selector

        if not UsingTool:  # Only when the user is not doing anything, tools can be selected. Preventing tools switching when drawing fast

            #####################################################################
            # Music Toggle
            if musicRect.collidepoint(mx, my):
                if click and volume == 1:
                    screen.blit(bkrd.subsurface(musicRect), musicRect)
                    screen.blit(musicOffIcon, musicRect)
                    volume = 0
                    mixer.music.set_volume(volume)

                elif click and volume == 0:
                    screen.blit(bkrd.subsurface(musicRect), musicRect)
                    screen.blit(musicIcon, musicRect)
                    volume = 1
                    mixer.music.set_volume(volume)

            #####################################################################
            # Other tools Selector

            elif openRect.collidepoint(mx, my):
                screen.blit(toolBorder, openRect)
                if click:
                    file = load_image()
                    if file:
                        drawn = True
                        screen.set_clip(canvasRect)
                        screen.blit(file, canvasRect)

            elif saveRect.collidepoint(mx, my):
                screen.blit(toolBorder, saveRect)
                if click:
                    save_image()

            elif spriteBarRect.collidepoint(mx, my):
                for i in range(10):
                    if Rect(605+i*60, 790, 60, 60).collidepoint(mx, my):
                        screen.blit(spriteBorder, (605+i*60, 790))
                        if mb[0] == 1:
                            spSelectedX = 605+i*60
                            spSelectedPage = spriteSurfaceNumber[0]

            elif bkBarRect.collidepoint(mx, my):
                for i in range(10):
                    if Rect(605+i*60, 700, 60, 60).collidepoint(mx, my):
                        screen.blit(spriteBorder, (605+i*60, 700))
                        if click:
                            try:
                                spSelectedX = "none"
                                screen.blit(backLibrary[10+i*60+bkSurfaceNumber[0]*600], canvasRect)
                                drawn = True
                            except:
                                print("error")
                                pass

            elif thicknessLineRect.collidepoint(mx, my) and mb[0] == 1:
                adder = mx-thicknessLineX[0] - 5
                if adder < 0:
                    adder = 1
                thickness = adder//2
                if othickness != thickness:
                    andyfont = pygame.font.Font("images/font/Andy.ttf", thickness)

            elif spriteleftRect.collidepoint(mx, my):
                if click:
                    spriteSurfaceNumber.append(spriteSurfaceNumber.pop(0))
                screen.blit(arrowSelected, spriteleftRect)

            elif spriterightRect.collidepoint(mx, my):
                if click:
                    spriteSurfaceNumber.insert(0, spriteSurfaceNumber.pop())
                screen.blit(RarrowSelected, spriterightRect)

            elif bkleftRect.collidepoint(mx, my):
                if click:
                    bkSurfaceNumber.append(bkSurfaceNumber.pop(0))
                screen.blit(arrowSelected, bkleftRect)

            elif bkrightRect.collidepoint(mx, my):
                if click:
                    bkSurfaceNumber.insert(0, bkSurfaceNumber.pop())
                screen.blit(RarrowSelected, bkrightRect)

            elif pencilRect.collidepoint(mx, my):  # Testing for the selection of different tools
                if mb[0] == 1:
                    tool = "pencil"  # Sets active tool if click
                    spSelectedX = "none"  # Resets the stamps to use tool
                screen.blit(toolBorder, pencilRect)  # Feedback

            elif brushRect.collidepoint(mx, my):
                if mb[0] == 1:
                    tool = "brush"
                    spSelectedX = "none"
                screen.blit(toolBorder, brushRect)

            elif eraserRect.collidepoint(mx, my):
                if mb[0] == 1:
                    tool = "eraser"
                    spSelectedX = "none"
                screen.blit(toolBorder, eraserRect)

            elif fillRect.collidepoint(mx, my):
                if mb[0] == 1:
                    tool = "fill"
                    spSelectedX = "none"
                screen.blit(toolBorder, fillRect)

            elif dropperRect.collidepoint(mx, my):
                if mb[0] == 1:
                    tool = "dropper"
                    spSelectedX = "none"
                screen.blit(toolBorder, dropperRect)

            elif markerRect.collidepoint(mx, my):
                if mb[0] == 1:
                    tool = "marker"
                    spSelectedX = "none"
                screen.blit(toolBorder, markerRect)

            elif textRect.collidepoint(mx, my):
                if mb[0] == 1:
                    tool = "text"
                    spSelectedX = "none"
                screen.blit(toolBorder, textRect)

            elif sprayRect.collidepoint(mx, my):
                if mb[0] == 1:
                    tool = "spray"
                    spSelectedX = "none"
                screen.blit(toolBorder, sprayRect)

            elif circleRect.collidepoint(mx, my):
                if mb[0] == 1:
                    tool = "circle"
                    spSelectedX = "none"
                screen.blit(toolBorder, circleRect)

            elif rectRect.collidepoint(mx, my):
                if mb[0] == 1:
                    tool = "rect"
                    spSelectedX = "none"
                screen.blit(toolBorder, rectRect)

            elif polygonRect.collidepoint(mx, my):
                if mb[0] == 1:
                    tool = "polygon"
                    spSelectedX = "none"
                screen.blit(toolBorder, polygonRect)

            elif lineRect.collidepoint(mx, my):
                if mb[0] == 1:
                    tool = "line"
                    spSelectedX = "none"
                screen.blit(toolBorder, lineRect)

            elif foregroundRect.collidepoint(mx, my):
                if mb[0] == 1:
                    activefb = "foreground"  # Sets active color
                screen.blit(toolBorder, foregroundRect)

            elif backgroundRect.collidepoint(mx, my):
                if mb[0] == 1:
                    activefb = "background"
                screen.blit(toolBorder, backgroundRect)

            elif sepiaRect.collidepoint(mx, my):
                if mb[0] == 1:
                    sepia(screen, canvasRect)  # Run the sepia filter
                    drawn = True  # Sepia filter is done so the user drew on the screen
                screen.blit(toolBorder, sepiaRect)

            elif grayRect.collidepoint(mx, my):
                if mb[0] == 1:
                    grayscale(screen, canvasRect)  # Runs the grayscale filter
                    drawn = True
                screen.blit(toolBorder, grayRect)

        #####################################################################
        # Display Selected Tool. Sets a border around the tool selected to tell the user what is selected at all times

        if spSelectedX != "none" and spSelectedPage == spriteSurfaceNumber[0]:
            screen.blit(spriteBorder, (spSelectedX, 790, 60, 60))

        if tool == "pencil":
            screen.blit(toolBorder, pencilRect)

        elif tool == "brush":
            screen.blit(toolBorder, brushRect)

        elif tool == "eraser":
            screen.blit(toolBorder, eraserRect)

        elif tool == "fill":
            screen.blit(toolBorder, fillRect)

        elif tool == "dropper":
            screen.blit(toolBorder, dropperRect)

        elif tool == "marker":
            screen.blit(toolBorder, markerRect)

        elif tool == "text":
            screen.blit(toolBorder, textRect)

        elif tool == "spray":
            screen.blit(toolBorder, sprayRect)

        elif tool == "circle":
            screen.blit(toolBorder, circleRect)

        elif tool == "rect":
            screen.blit(toolBorder, rectRect)

        elif tool == "polygon":
            screen.blit(toolBorder, polygonRect)

        elif tool == "line":
            screen.blit(toolBorder, lineRect)

        #####################################################################
        # Stamp usage
        screen.set_clip(canvasRect)

        if mb[0] == 1 and spSelectedX != "none" and canvasRect.collidepoint(mx, my):
            drawn = True
            tool = "no tool"
            screen.blit(canvasBuff, canvasRect)
            try:
                currentSprite = spriteLibrary[spSelectedX-595+spSelectedPage*600]  # Decode the sprite number and gets that stamp from the stamp library
                screen.blit(currentSprite, (mx-currentSprite.get_width()//2, my-currentSprite.get_height()//2))  # Blits the stamp with the mouse centered on the stamp
            except:
                pass

        #####################################################################
        # Tool Usage

        if textMode:  # If the text tool is active
            if canvasRect.collidepoint(mx, my) and click:
                textx, texty = mx, my  # Allows the user to change the location of the text
            screen.blit(canvasBuff, canvasRect)  # Refreshes the screen
            textSurf = text_tool(text, currentColor)  # Gets a surface with the text
            screen.blit(text_tool(text, currentColor), (textx, texty))  # blit it

        if canvasRect.collidepoint(mx, my) and mb[0] == 1:  # Drawing on the screen/ Using the actual tool

            if tool == "pencil":  # Testing for the tool
                drawn = True  # The user have drawn something
                if [mx, my] == [omx, omy]:  # test if the user moved his or her mouse
                    screen.set_at([mx, my], currentColor)  # If no movement, sets 1 pixel to the color
                else:
                    draw.aaline(screen, currentColor, [omx, omy], [mx, my], 1)  # Movement. Draw a line from the old mouse position to the new mouse positions

            elif tool == "brush":
                drawn = True
                continuous_line(screen, currentColor, [omx, omy], [mx, my], max(1, thickness//2))  # Runs the tool function

            elif tool == "eraser":
                drawn = True
                continuous_line(screen, WHITE, [omx, omy], [mx, my], max(1, thickness//2))

            elif tool == "fill":
                drawn = True
                fill_bucket(mx, my, currentColor, screen.get_at((mx, my)), screen.subsurface(canvasRect).copy())

            elif tool == "dropper":
                if activefb == "foreground":  # Finds the active color
                    foreground = screen.get_at((mx, my))  # Sets the color
                else:
                    background = screen.get_at((mx, my))

            elif tool == "marker":
                drawn = True
                brushHead = brushHeadTemplate.copy()  # Creating a brush head from template
                draw.circle(brushHead, (currentColor[0], currentColor[1], currentColor[2], 10), (101, 101), max(1, thickness//2))  # Draws the actual brush head for blitting

                marker(mx-101, my-101, omx-101, omy-101, brushHead, screen)

            elif tool == "text" and not textMode:  # Only run this tool if the tool is not being used
                toolRequire = True  # This tool requires the program to not copy the screen every mouse down
                textMode = True  # Tool is active
                text = ""  # Reset the text to be blit
                textx, texty = mx, my  # Sets the location of the text

            elif tool == "spray":
                drawn = True
                continuous_line(screen, currentColor, [omx, omy], [mx, my], max(1, thickness // 2), True)

            elif tool == "circle":
                drawn = True
                screen.blit(canvasBuff, canvasRect)
                cir_mode = True  # Circle tool is active
                draw_cir(screen, mx, my, currentColor, thickness // 2, shift_pressed)

            elif tool == "polygon":
                drawn = True
                screen.blit(canvasBuff, canvasRect)
                polymode = toolRequire = True
                draw_polygon(screen, mx, my, currentColor, thickness)

            elif tool == "rect":
                drawn = True
                screen.blit(canvasBuff, canvasRect)
                rect_mode = True
                draw_rect(screen, mx, my, currentColor, thickness // 2, shift_pressed)

            elif tool == "line":
                drawn = True
                screen.blit(canvasBuff, canvasRect)
                line_mode = True
                line(screen, mx, my, currentColor, max(1, thickness // 2))

        if tool == "circle" and mb[0] == 0:  # Release to stop tool
            cir_mode = False  # Sets tool status
            start_cir = []  # Reset starting point list for new circle

        elif tool == "rect" and mb[0] == 0:
            rect_mode = False
            start_rect = []

        elif (tool != "polygon" and polymode) or enter:
            polymode = toolRequire = False
            polypoints = []

        elif tool == "line" and mb[0] == 0:
            line_mode = False
            start_line = []

        if (tool != "text" or enter) and textMode:  # Stop text tool
            screen.blit(canvasBuff, canvasRect)  # Final refresh to blit the Text
            screen.blit(text_tool(text, currentColor), (textx, texty))
            canvasBuff = screen.subsurface(canvasRect).copy()  # Refresh buffer
            drawn = True
            toolRequire = False
            textMode = False  # Reset mode

        if Utility_Rect.collidepoint(mx, my) and mb[0] == 1 and not UsingTool:  # Using the utility box
            if colorpickerRect.collidepoint(mx, my):  # Selected color from color picker
                if activefb == "foreground":  # Active color is foreground
                    foreground = screen.get_at((mx, my))
                else:
                    background = screen.get_at((mx, my))

    #########################################################################
    omx, omy = mx, my  # Keeps the old mouse cords for future use
    othickness = thickness  # Keeps the old thickness

    #########################################################################
    # Cursor
    curBuff = screen.copy()  # Gets a buffer of the entire screen

    if canvasRect.collidepoint(mx, my) and tool != "no tool":  # Detects whether a tool is active or not

        if tool in ['brush', 'eraser', 'spray', 'marker']:  # If the tool is one of the tools in the list, just draw a circle for the circle
            draw.circle(screen, BLACK, [mx, my], max(1, thickness // 2), 1)

        elif tool in ['polygon', 'circle', 'line', 'rect']:  # If the tool is one of the tools in the list, gets the icon for the tool from the library and blits it under the cursor
            screen.blit(cursor, (mx, my))
            screen.blit(toolSurfaceLib[tool], (mx + cursor.get_height(), my + cursor.get_width()))

        elif tool == "fill":  # Same as above
            screen.blit(cursor, (mx, my))
            screen.blit(toolSurfaceLib["painter"], (mx + cursor.get_height(), my + cursor.get_width()))

        elif tool == "text":  # text tool needs cursorText to be blit every run
            screen.blit(cursorText, (mx, my))

        elif tool in ["dropper", "pencil"]:  # Dropper and pencil require calculations to blit the tool in the correct position
            screen.blit(toolSurfaceLib[tool], (mx, my-toolSurfaceLib[tool].get_height()))

        display.flip()  # Updates the screen
        screen.blit(curBuff, (0, 0))  # Resets the screen for next frame
    else:  # No tool
        screen.set_clip(None)  # No clip because the user is not drawing
        screen.blit(cursor, (mx, my))  # Blit the cursor to the correct location

        display.flip() # Updates the screen
        screen.blit(curBuff, (0, 0))  # Resets the screen for next frame

    screen.set_clip(None)  # Reset clipping
    #########################################################################

quit()  # Quits the program completely
